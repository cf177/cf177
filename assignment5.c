#include <stdio.h>
int input()
{
 int n;
 printf("Enter a number to be reversed\n");
 scanf("%d",&n);
 return n;
}
int process(int n)
{
 int rev=0,rem;
 while(n!=0)
 {
  rem=n%10;
  rev=(rev*10)+rem;
  n=n/10;
 }
 return rev;
}
int output(int n,int rev)
{
 printf("The reverse of %d is %d\n",n,rev);
 return 0;
}
int main()
{
 int n,rev;
 n=input();
 rev=process(n);
 output(n,rev);
 return 0;
}