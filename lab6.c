#include <stdio.h>
void input(int* m,int* n)
{
    printf("Enter total students and courses\n");
    scanf("%d%d",m,n);
}
void array_input(int x,int y,int a[x][y])
{
    int i,k;
    printf("Enter the marks\n");
    for(i=0;i<x;i++)
    {
        for(k=0;k<y;k++)
        {
            scanf("%d",&a[i][k]);
        }
        printf("\n");
    }
}
void highest(int x,int y,int a[x][y],int hn[y])
{
    int i,k;
    for(i=0;i<y;i++)
    {
        hn[i]=a[0][i];
        for(k=1;k<x;k++)
        {
            if(a[k][i]>hn[i])
            {
                hn[i]=a[k][i];
            }
        }
    }
}
void output(int y,int hn[y])
{
    int i;
    for(i=0;i<y;i++)
    {
        printf("Highest marks in course %d is %d\n",i+1,hn[i]);
    }
}
int main()
{
	int m,n;
    input(&m,&n);
    int a[m][n];
    array_input(m,n,a);
    int hn[n];
    highest(m,n,a,hn);
    output(n,hn);
    return 0;
}
//write your code here