#include <stdio.h>
void input(int* m,int* n)
{
    printf("Enter total salesman and products\n");
    scanf("%d%d",m,n);
}
void array_input(int x,int y,int a[x][y])
{
    int i,k;
    printf("Enter the sales of %d salesman and %d products\n",x,y);
    for(i=0;i<x;i++)
    {
        for(k=0;k<y;k++)
        {
            scanf("%d",&a[i][k]);
        }
        printf("\n");
    }
}
int add(int s,int t)
{
    t=t+s;
    return t;
}
void output(int x,int y,int a[x][y])
{
    int i,k,total_sales,product_sales;
    for(i=0;i<x;i++)
    {
        total_sales=0;
        for(k=0;k<y;k++)
        {
            total_sales=add(a[i][k],total_sales);
        }
        printf("Total sales of salesman %d is %d\n",i+1,total_sales);
    }
    for(i=0;i<y;i++)
    {
        product_sales=0;
        for(k=0;k<x;k++)
        {
            product_sales=add(a[k][i],product_sales);
        }
        printf("Total sales of product %d is %d\n",i+1,product_sales);
    }
}
int main()
{
	int m,n;
    input(&m,&n);
    int a[m][n];
    array_input(m,n,a);
    output(m,n,a);
    return 0;
}
//Enter your code here