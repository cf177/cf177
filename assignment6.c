#include <stdio.h>
void insert(int prime[10])
{
	int i,k,pos;
	for(i=0;i<10;i++)
	{
		if(prime[i]>7)
		{
            pos=i;
			for(k=8;k>i-1;k--)
			{
				prime[k+1]=prime[k];
			}
            break;
        }
    }
        prime[pos]=7;
}
void output(int prime[10])
{
	int i;
	printf("The updated array is\n");
	for(i=0;i<10;i++)
	{
		printf("%d\n",prime[i]);
	}
}
int main()
{
	int prime[10]={2,3,5,11,13,17,19,23};
	insert(prime);
	output(prime);
	return 0;
}