#include <stdio.h>
int input()
{
    int a;
    printf("Enter an integer\n");
    scanf("%d",&a);
    return a;
}
int process(int a,int b,int c)
{
    int sum;
    sum=a+b+c;
    return sum;
}
int output(int a,int b,int c,int sum)
{
    printf("Sum of %d,%d and %d is %d\n",a,b,c,sum);
    return 0;
}
int main()
{
    int n1,n2,n3,res;
    n1=input();
    n2=input();
    n3=input();
    res=process(n1,n2,n3);
    output(n1,n2,n3,res);
    return 0;
}
    