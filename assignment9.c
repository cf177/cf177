#include<stdio.h>
struct Book
{
	char title[25];
	char author[25];
	float price;
	int pages;
};
struct Book input1()
{
    struct Book b; 
	printf("Enter Title,Author,price and pages\n");
	gets(b.title);
    getchar();
	gets(b.author);
	scanf("%f%d",&b.price,&b.pages);
    return b;
}
struct Book input2()
{
    struct Book b; 
	printf("Enter Title,Author,price and pages\n");
    getchar();
	gets(b.title);
    getchar();
	gets(b.author);
	scanf("%f%d",&b.price,&b.pages);
    return b;
}
struct Book price(struct Book b1,struct Book b2)
{
	if(b1.price>b2.price)
	return b1;
	else
	return b2;
}
struct Book page(struct Book b1,struct Book b2)
{
	if(b1.pages>b2.pages)
	return b2;
	else
	return b1;
}
void output(struct Book p,struct Book np)
{
	printf("The title of the book which is expensive is %s\n",p.title);
	printf("The author of the book which has less number of pages is %s\n",np.author);
}
int main()
{
	struct Book b1,b2,np,p;
	b1=input1();
    b2=input2();
    p=price(b1,b2);
	np=page(b1,b2);
	output(p,np);
	return 0;
}