#include <stdio.h>
#include <math.h>
void input(float* x1,float*y1,float* x2,float* y2)
{
	printf("Enter the value of (x1,y1)\n");
    scanf("%f%f",x1,y1);
    printf("Enter the value of (x2,y2)\n");
    scanf("%f%f",x2,y2);
} 
float process(float a,float b,float c,float d)
{
    float distance;
    distance=sqrt(pow((c-a),2)+pow((d-b),2));
    return distance;
}
void output(float a,float b,float c,float d,float distance)
{
    printf("Distance between (%f,%f) and (%f,%f) is %f",a,b,c,d,distance);
}
int main()
{
    float x1,y1,x2,y2,distance;
    input(&x1,&y1,&x2,&y2);
    distance=process(x1,y1,x2,y2);
    output(x1,y1,x2,y2,distance);
    return 0;
}