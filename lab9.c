#include<stdio.h>
void input(int* n1,int* n2)
{
 printf("Enter 2 numbers\n");
 scanf("%d%d",n1,n2);
}
void swap(int *n1,int *n2)
{
 *n1=*n1+*n2;
 *n2=*n1-*n2;
 *n1=*n1-*n2;
}
int output(int x,int y)
{
 printf("Number before swapping is %d and %d\n",y,x);
 printf("Number after swapping is %d and %d\n",x,y);
 return 0;
}
int main()
{
 int n1,n2;
 input(&n1,&n2);
 swap(&n1,&n2);
 output(n1,n2);
 return 0;
}
