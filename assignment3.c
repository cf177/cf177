#include<stdio.h>
int input1()
{
    int m;
    printf("Enter marks in Maths\n");
    scanf("%d",&m);
    return m;
}
int input2()
{
    int m;
    printf("Enter marks in Physics\n");
    scanf("%d",&m);
    return m;
}
int input3()
{
    int m;
    printf("Enter marks in ECE\n");
    scanf("%d",&m);
    return m;
}
int input4()
{
    int m;
    printf("Enter marks in EME\n");
    scanf("%d",&m);
    return m;
}
int input5()
{
    int m;
    printf("Enter marks in CCP\n");
    scanf("%d",&m);
    return m;
}
int process(int m)
{
    if(m>=90 && m>=100)
        return 'S';
    else if(m>=81 && m<=90)
        return 'A';
    else if(m>=71 && m<=80)
        return 'B';
    else if(m>=61 && m<=70)
        return 'C';
    else if(m>=51 && m<=60)
        return 'D';
    else if(m>=41 && m<=50)
        return 'E';
    else
        return 'F';
}
int output(char ch)
{
    if(ch=='S')
        printf("Your grade is S\n");
    if(ch=='A')
        printf("Your grade is A\n");
    if(ch=='B')
        printf("Your grade is B\n");
    if(ch=='C')
        printf("Your grade is C\n");
    if(ch=='D')
        printf("Your grade is D\n");
    if(ch=='E')
        printf("Your grade is E\n");
    if(ch=='F')
        printf("Your grade is F\n");
    return 0;
}
int main()
{
    int maths,physics,ece,eme,ccp;
    char ma,phy,ec,em,cc;
    maths=input1();
    ma=process(maths);
    output(ma);
    physics=input2();
    phy=process(physics);
    output(phy);
    ece=input3();
    ec=process(ece);
    output(ec);
    eme=input4();
    em=process(eme);
    output(em);
    ccp=input5();
    cc=process(ccp);
    output(cc);
    return 0;
}//Enter your code here