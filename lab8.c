#include<stdio.h>
struct Employee
{
    char name[20];
    int id;
    int salary;
    int d,m,y;
};
struct Employee input()
{
 struct Employee e;
 printf("Enter Name\n");
 gets(e.name);
 printf("Enter Employee id\n");
 scanf("%d",&e.id);
 printf("Enter salary\n");
 scanf("%d",&e.salary);
 printf("Enter day,month,year of joining\n");
 scanf("%d%d%d",&e.d,&e.m,&e.y);
 return e;
}
void output(struct Employee e)
{
 printf("Name:%s\nEmployee id:%d\nSalary:%d\nDate of Joining:%d/%d/%d",e.name,e.id,e.salary,e.d,e.m,e.y);
}
int main()
{
 struct Employee e;
 e=input();
 output(e);
 return 0;
}