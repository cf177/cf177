#include <stdio.h>
int total_input()
{
    int n;
    printf("Enter total inputs\n");
    scanf("%d",&n);
    return n;
}
void inputs(int n,int a[n],int* s)
{
    int i;
    printf("Enter %d elements\n",n);
    for(i=0;i<n;i++)
    scanf("%d",&a[i]);
    printf("Enter the element to be searched\n");
    scanf("%d",s);
}
int search(int m,int a[m],int e)
{
    int i;
    for(i=0;i<m;i++)
    {
        if(a[i]==e)
        {
            break;
        }
    }
    return i;
}
void output(int n,int i)
{
    if(i==n)
    printf("Element not found\n");
    else
    printf("Found in %d\n",i);    
}
int main(int argc, char **argv)
{
	int n,s,i;
    n=total_input();
    int a[n];
    inputs(n,a,&s);
    i=search(n,a,s);
    output(n,i);
	return 0;
}
