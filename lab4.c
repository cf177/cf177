//write your code here
#include<stdio.h>

int input()
{
    int n;
    printf("Enter a number to calculate sum of digits\n");
    scanf("%d",&n);
    return n;
}
int add(int n)
{
    int sum=0,i,rem;
    while(n!=0)
    {
        rem=n%10;
        sum+=rem;
        n=n/10;
    }
    return sum;
}    
int output(int n,int sum)
{
    printf("The sum of digits of %d is %d\n",n,sum);
    return 0;
}
int main()
{
    int n,sum;
    n=input();
    sum=add(n);
    output(n,sum);
    return 0;
}
