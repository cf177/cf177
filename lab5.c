#include <stdio.h>
int total_input()
{
    int n;
    printf("Enter total inputs\n");
    scanf("%d",&n);
    return n;
}
void array_input(int n,int a[n])
{
    int i;
    printf("Enter %d numbers\n",n);
    for(i=0;i<n;i++)
    scanf("%d",&a[i]);
}
void swap(int n,int a[n],int* spos,int* lpos)
{
    int i,sn,ss,hn,hs;
    sn=a[0];ss=0;
    for(i=1;i<n;i++)
    {
        if(a[i]<sn)
        {
            sn=a[i];
            ss=i;
        }
    }
    hn=a[0];hs=0;
    for(i=1;i<n;i++)
    {
        if(a[i]>sn)
        {
            hn=a[i];
            hs=i;
        }
    }
    a[hs]=sn;
    *spos=hs;
    a[ss]=hn;
    *lpos=ss;
}
void output(int n,int a[n],int ss,int hs)
{
    int i;
    printf("The swapped array is\n");
    for(i=0;i<n;i++)
    printf("%d ",a[i]);
    printf("\nThe position of smallest number is %d and position of largest number is %d\n",ss,hs); 
}
int main()
{
	int n,spos,lpos;
    n=total_input();
    int a[n];
    array_input(n,a);
    swap(n,a,&spos,&lpos);
    output(n,a,spos,lpos);
    return 0;
}
