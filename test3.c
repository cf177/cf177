#include<stdio.h>
#include<math.h>
#include<stdlib.h>
struct complex
{
	float real,img;
};
void input(float *a,float *b,float *c)
{
	printf("Enter the coefficent of x^2:");
	scanf("%f",a);
	printf("Enter the coefficent of x:");
	scanf("%f",b);
	printf("Enter the constant:");
	scanf("%f",c);
}

float compute(float x,float y,float z,struct complex *root1,struct complex *root2,float *d)
{
	*d=((y*y)-(4*x*z));
    if(x!=0)
    {
	if(d>0)
	{
		root1->real=(-y+sqrt(*d))/(2*x);
		root2->real=(-y-sqrt(*d))/(2*x);
	}
	else if(d==0)
	{
		root1->real=(-y)/(2*x);
		root2->real=(-y)/(2*x);
	}
	else
	{   
		root1->real=(-y)/(2.0*x);
		root1->img=((float)sqrt(-(*d)/(2*x)));
	}
    	return 1;
    }
	else
	{
	    return 0;
	}
}

void output(struct complex root1,struct complex root2,int flag,float dis)
{
	if(flag==1)
	{
		if(dis>0)
        {
		printf("Real and unequal roots \nThe roots are:"); 
		printf("Root1=%f\nRoot2=%f\n",root1.real,root2.real);
        }
        else if(dis==0)
        {
            printf("Real and equal root \n");
            printf("Root1=Root2=%f\n",root1.real);
        }
        else 
        {
            printf("Imaginary roots\n");
            printf("Root1=%f + %f i\n",root1.real,root1.img);
            printf("Root2=%f - %f i\n",root1.real,root1.img);
        }
    }
	else
	{
        printf("It is not a quadratic equation\n");
    }
}
int main()
{
	float a,b,c,d,flag;
	struct complex root1,root2;
	input(&a,&b,&c);
	flag=compute(a,b,c,&root1,&root2,&d);
	output(root1,root2,flag,d);
	return 0;
}
