#include <stdio.h>
int total_input()
{
    int n;
    printf("Enter total inputs\n");
    scanf("%d",&n);
    return n;
}
void array_input(int n,int a[n])
{
    int i;
    printf("Enter %d numbers\n",n);
    for(i=0;i<n;i++)
    scanf("%d",&a[i]);
}
float average(int n,int a[n])
{
    int i,sum;
    float avg;
    sum=0;
    for(i=0;i<n;i++)
    {
        sum+=a[i];
    }
    avg=(float)sum/n;
    return avg;
    
}
void output(float avg)
{
    printf("The average is %f\n",avg);
}
int main()
{
	int n;
    float avg;
    n=total_input();
    int a[n];
    array_input(n,a);
    avg=average(n,a);
    output(avg);
    return 0;
}
