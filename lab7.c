#include<stdio.h>
#include<stdlib.h>
void input(char s1[100],char s2[100])
{
	printf("Enter 1st string\n");
	scanf("%s",s1);
    printf("Enter 2nd string\n");
    scanf("%s",s2);
}
void concat(char s1[100],char s2[100],char s3[100])
{
	int i=0,k=0;
	while(s1[i]!='\0')
	{
		s3[i]=s1[i];
		i++;
		k++;
	}
	while(s2[k-i]!='\0')
	{
		s3[k]=s2[k-i];
		k++;
	}
	s3[k]='\0';
}
void output(char s3[100])
{
	printf("The concatenated string is %s\n",s3);
}
int main()
{
	char s1[100];
	char s2[100];
	char s3[100];
    input(s1,s2);
	concat(s1,s2,s3);
	output(s3);
	return 0;
}