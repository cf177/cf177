#include <stdio.h>
int input1()
{
    int h;
    printf("Enter number of hours:\n");
    scanf("%d",&h);
    return h;
}
int input2()
{
    int m;
    printf("Enter minutes between 1 to 59:\n");
    scanf("%d",&m);
    return m;
}
int process(int h,int m)
{
    int min;
    min=(h*60)+m;
    return min;
}
int output(int h,int m,int min)
{
    printf("The converted time from hours and minutes to total minutes of %dhours and %dminutes is %d\n",h,m,min);
    return 0;
}
int main()
{
    int hours,minutes,total_minutes;
    hours=input1();
    minutes=input2();
    total_minutes=process(hours,minutes);
    output(hours,minutes,total_minutes);
    return 0;
}
//write your code here