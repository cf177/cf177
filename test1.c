
#include<stdio.h>
void input(int* p,int* t,int* r)
{
 printf("Enter the principle, time and rate in percentage\n");
 scanf("%d%d%d",p,t,r);
}
float interest(int principle,int time,int rate)
{
 float si;
 si=(principle*time*rate)/100.0;
 return si;
}
void output(int principle,int time,int rate,float si)
{
 printf("The simple interest for\nPrinciple=%d\nTime=%d\nRate=%d is %f\n",principle,time,rate,si);
}
int main()
{
 int p,t,r;
 float si;
 input(&p,&t,&r);
 si=interest(p,t,r);
 output(p,t,r,si);
 return 0;
}//write your code here