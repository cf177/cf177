#include<stdio.h>
float input()
{
    float x;
    printf("Enter the radius of the circle\n");
    scanf("%f",&x);
    return x;
}
float process(float r)
{
    float a;
    a=3.14*r*r;
    return a;
}
float output(float x,float a)
{
    printf("The area of the circle of radius %f=%f\n",x,a);
    return 0.0;
}
int main()
{
    float radius,area;
    radius=input();
    area=process(radius);
    output(radius,area);
}
