#include<stdio.h>
void input(int *n1,int *n2)
{
    printf("Enter 2 numbers\n");
    scanf("%d%d",n1,n2);
}
void operation(int *n1,int *n2,int *add,int *sub,int *mul,float *div,int *rem)
{
    *add=*n1 + *n2;
    *sub=*n1 - *n2;
    *mul=*n1 * *n2;
    *div=(float)*n1 / *n2;
    *rem=*n1 % *n2;
}
int output(int sum,int dif,int pro,float quo,int remainder)
{
    printf("Sum=%d\n",sum);
    printf("Difference=%d\n",dif);
    printf("Product=%d\n",pro);
    printf("Quotient=%f\n",quo);
    printf("Remainder=%d\n",remainder);
}
int main()
{
    int n1,n2,add,sub,mul,rem;
    float div;
    input(&n1,&n2);
    operation(&n1,&n2,&add,&sub,&mul,&div,&rem);
    output(add,sub,mul,div,rem);
    return 0;
}
