//write your code here
#include<stdio.h>
struct Student
{
	int roll_no;
	char name[20];
	char section;
	char department[100];
	int fees;
	int marks;
};
struct Student input()
{
    struct Student s; 
	printf("Enter Roll no.,name,section,department,fees,marks of student\n");
	scanf("%d",&s.roll_no);
    getchar();
	gets(s.name);
	scanf(" %c",&s.section);
    getchar();
	gets(s.department);
	scanf("%d%d",&s.fees,&s.marks);
    return s;
}
struct Student high(struct Student s1,struct Student s2)
{
	if(s1.marks>s2.marks)
	return s1;
	else
	return s2;
}
void output(struct Student h)
{
	printf("The details of student with higher marks is:\nRoll no:%d\nName:%s\nSection:%c\nDepartment:%s\nFees:%d\nMarks:%d\n",h.roll_no,h.name,h.section,h.department,h.fees,h.marks);
}
int main()
{
	struct Student s1,s2,h;
	s1=input();
    s2=input();
	h=high(s1,s2);
	output(h);
	return 0;
}