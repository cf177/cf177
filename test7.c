#include <stdio.h>
void input(int* m,int* n)
{
    printf("Enter rows and columns\n");
    scanf("%d%d",m,n);
}
void array_input(int x,int y,int a[x][y])
{
    int i,k;
    printf("Enter the matrix\n");
    for(i=0;i<x;i++)
    {
        for(k=0;k<y;k++)
        {
            scanf("%d",&a[i][k]);
        }
        printf("\n");
    }
}
void transpose(int x,int y,int a[x][y],int t[y][x])
{
    int i,k;
    for(i=0;i<x;i++)
    {
        for(k=0;k<y;k++)
        {
            t[k][i]=a[i][k];
        }
    }
}
void output(int x,int y,int t[y][x])
{
    int i,k;
    printf("The transpose is\n");
    for(i=0;i<y;i++)
    {
        for(k=0;k<x;k++)
        {
            printf("%d ",t[i][k]);
        }
        printf("\n");
    }
}
int main()
{
	int m,n;
    input(&m,&n);
    int a[m][n];
    int t[n][m];
    array_input(m,n,a);
    transpose(m,n,a,t);
    output(m,n,t);
    return 0;
}
//write your code here